export class Bed {
    _id: string;
    room_number: number;
    number: number;
    used: number;
}
