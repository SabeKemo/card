import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormControl } from '@angular/forms';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { PatientsComponent } from './patients.component';
import { PatientNewComponent } from '../patient-new/patient-new.component';
import { PatientEditComponent } from '@app/patient-edit/patient-edit.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'patients',
      component: PatientsComponent,
      data: {
        title: extract('Patients')
      },
    },
    {
      path: 'patients/new',
      component: PatientNewComponent,
      data: {
        title: extract('New patient')
      },
    },
    {
      path: 'patients/new/:bed_id',
      component: PatientNewComponent,
      data: {
        title: extract('New patient')
      },
    },
    {
      path: 'patients/edit/:_id',
      component: PatientEditComponent,
      data: {
        title: extract('Edit patient')
      },
    }
    
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PatientsRoutingModule { }
