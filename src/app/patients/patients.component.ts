import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

import { environment } from '@env/environment';

import { KinveyApiService } from '../kinvey-api.service';
import { Patient } from '@app/patient';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {

  version: string = environment.version;

  patients: Patient[] = [];

  displayedColumns: string[] = [
    "name",
    "treatment",
    "demographics",
    "allergies",
    "blood_type",
    "diagnosis",
    "weight",
    "sex",
    "age",
    "actions",
  ];

  dataSource = new PatientsDataSource(this.kinveyService);

  constructor(private kinveyService: KinveyApiService) { }

  applyFilter(filterValue: string) {

    console.log(filterValue);
    // TODO: implement
  }

  ngOnInit() {}
}

export class PatientsDataSource extends DataSource<any> {
  constructor(private kinveyService: KinveyApiService) {
    super();
  }
  connect(): Observable<Patient[]> {
    return this.kinveyService.getPatients();
  }

  disconnect() { }
}
