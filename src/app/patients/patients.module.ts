import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';


//import { MaterialModule } from '@app/material.module';
import { PatientsRoutingModule } from './patients-routing.module';
import { PatientsComponent } from './patients.component';
import { MatRippleModule, MatInputModule, MatFormFieldModule, MatButtonModule, MatTreeModule, MatTableModule, MatGridListModule, MatListModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { MaterialModule } from '@app/material.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    PatientsRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTreeModule,
    CdkTableModule,
    MatTableModule,
    MatListModule,
    MatGridListModule,
  ],
  exports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    PatientsRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatListModule,
    MatGridListModule,
  ],
  declarations: [
    PatientsComponent
  ]
})
export class PatientsModule { }
