import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientsComponent } from './patients/patients.component';
import { PatientNewComponent } from './patient-new/patient-new.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';

const routes: Routes = [
  // Fallback when no prior route is matched
  {
    path: 'patients',
    component: PatientsComponent,
    children: [
      {
        path: 'new',
        component: PatientNewComponent
      },
      {
        path: 'edit/:_id',
        component: PatientEditComponent
      }
    ]
  },
  { path: '**', redirectTo: '/rooms', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
