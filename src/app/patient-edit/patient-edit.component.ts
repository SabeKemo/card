import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';


import { KinveyApiService } from '../kinvey-api.service';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs';
import { Patient } from '@app/patient';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ActivatedRoute } from '@angular/router';

export interface Bed {
  id: string;
  number: string;
}

export interface Room {
  number: string;
  beds: Bed[];
}

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.scss']
})
export class PatientEditComponent implements OnInit {

  constructor(private route: ActivatedRoute, private kinveyService: KinveyApiService) {

  }

  id: string;
  rooms: Room[] = [];
  model = new Patient();
  valid = true;

  file: File;

  blob: Blob;

  submitted = false;
  initial_bed = '';
  // public uploader:FileUploader = edit FileUploader({
  //   url: ""
  // });

  convertToBlob(file: any) {

    let reader = new FileReader();
    let self = this;
    reader.addEventListener("load", function () {

      self.model.patient_image = reader.result;
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  validateForm() {

    for (let prop in this.model) {
      if (this.model[prop] == "") {
        this.valid = false;
        return false;
      }
    }
  }


  handleFileInput(files: File[]) {

    this.file = files[0];
    //console.log(this.file)
    this.convertToBlob(this.file);

    console.log(this.model.patient_image);

    // let block = this.model.patient_image.split(";");
    // const contentType = block[0].split(":")[1];// In this case "image/gif"
    // console.log("contentType", contentType);
    // const b64Data = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
    // console.log("b64Data", b64Data);

    // const blob = this.b64toBlob(b64Data, contentType);

    // console.log("Blob", blob);
    // const blobUrl = URL.createObjectURL(blob);
    // console.log("blobUrl", blobUrl);

    return false;
  }

  b64toBlob(b64Data: any, contentType: any = '', sliceSize: any = 512) {

    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


  onSubmit() {

    this.submitted = true;

    // this.kinveyService.getUploadUri().subscribe((res) => {
    //   console.log(res);
    //   console.log("google cloud url gathered")
    //   console.log(res["_uploadURL"])
    //   this.kinveyService.storeFileToGoogleCloud(res["_uploadURL"], this.file).subscribe((res) => {
    //     console.log("creating image successfull");
    //     console.log(res["_uploadURL"]);
    //     console.log(res)
    //   });
    // });

    this.kinveyService.updatePatient(this.model, this.id).subscribe(suc => {
      console.log(suc);
      if (this.model.bed_id.trim() !== '') {
        this.kinveyService.getBed(suc.bed_id).subscribe(bed => {
          bed.used = 1;
          this.kinveyService.updateBed(bed, bed._id).subscribe(response => {
            console.log(response);
            // Free bed if a new one has been selected
            if (this.initial_bed.trim() !== '' && this.initial_bed !== this.model.bed_id) {
              this.kinveyService.getBed(this.initial_bed).subscribe(bedprev => {
                bedprev.used = 0;
                this.kinveyService.updateBed(bedprev, bedprev._id).subscribe(responseprev => {
                  console.log(responseprev);
                }
                );
              });
            }
          }
          );
        });
      }
    }, err => {
      console.log(err);
    });

    return false;
  }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('_id');
    const self = this;

    this.kinveyService.getPatient(this.id).subscribe((patientres) => {
      self.model = patientres;
      this.initial_bed = patientres.bed_id;

      this.kinveyService.getFreeRooms(patientres.bed_id).subscribe(res => {
        res.forEach(obj => {
          let foundroom = false;
          this.rooms.forEach((fr, idx) => {
            if (fr.number === obj.room_number.toString()) {
              foundroom = true;
              this.rooms[idx].beds.push({
                id: obj._id,
                number: obj.number.toString()
              });
            }
          });

          if (!foundroom) {
            const newobj: Room = {
              number: obj.room_number.toString(),
              beds: [{
                id: obj._id,
                number: obj.number.toString()
              }]
            };
            this.rooms.push(newobj);
          }
        });
      });
    });
  }
}


