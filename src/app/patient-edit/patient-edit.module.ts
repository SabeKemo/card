import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

//import { MaterialModule } from '@app/material.module';
//import { PatientNewRoutingModule } from './patient-new-routing.module';
import { PatientEditComponent } from './patient-edit.component';
import { MatRippleModule, MatInputModule, MatFormFieldModule, MatButtonModule, MatTreeModule, MatTableModule, MatSelectModule, MatOption, MatOptgroup, MatOptionModule, MatOptgroupBase, MatListModule, MatGridListModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { MaterialModule } from '@app/material.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTreeModule,
    MatSelectModule,
    MatOptionModule,
    CdkTableModule,
    MatTableModule,
    FormsModule,
    MatListModule,
    MatGridListModule,
    FileUploadModule
  ],
  exports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,  
    MatTableModule,
    MatSelectModule,
    MatOptionModule,
    FormsModule,
    MatListModule,
    MatGridListModule,
  ],
  declarations: [
    PatientEditComponent
  ]
})
export class PatientEditModule { }