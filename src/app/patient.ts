export class Patient {
    _id: string;
    name: string  = "";
    treatment: string  = "";
    demographics: string  = "";
    allergies: string  = "";
    blood_type: string  = "";
    diagnosis: string  = "";
    weight: string  = "";
    sex: string  = "";
    age: string  = "";
    patient_image: any  = "";
    bed_id: string  = "";
}
