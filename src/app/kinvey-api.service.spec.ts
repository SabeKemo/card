import { TestBed } from '@angular/core/testing';

import { KinveyApiService } from './kinvey-api.service';

describe('KinveyApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KinveyApiService = TestBed.get(KinveyApiService);
    expect(service).toBeTruthy();
  });
});
