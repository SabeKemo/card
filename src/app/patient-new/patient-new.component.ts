import { Component, OnInit } from '@angular/core';
import { KinveyApiService } from '../kinvey-api.service';
import { Patient } from '@app/patient';
import { ActivatedRoute } from '@angular/router';


export interface Bed {
  id: string;
  number: string;
}

export interface Room {
  number: string;
  beds: Bed[];
}

@Component({
  selector: 'app-patient-new',
  templateUrl: './patient-new.component.html',
  styleUrls: ['./patient-new.component.scss']
})
export class PatientNewComponent implements OnInit {

  model = new Patient();
  rooms: Room[] = [];
  submitted = false;
  valid = true;
  file: File;

  convertToBlob(file: any) {

    let reader = new FileReader();
    let self = this;
    reader.addEventListener("load", function () {

      self.model.patient_image = reader.result;
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  handleFileInput(files: any[]) {

    this.convertToBlob(files[0]);

    //    console.log(this.model.patient_image);
  }

  constructor(private route: ActivatedRoute, private kinveyService: KinveyApiService) {

  }

  validateForm() {

    for (let prop in this.model) {
      if (this.model[prop] == "") {
        this.valid = false;
        return false;
      }
    }
  }

  onSubmit() {

    this.submitted = true;
    // this.kinveyService.getUploadUri().subscribe((res) => {
    //   console.log(res);
    //   console.log("google cloud url gathered")
    //   console.log(res["_uploadURL"])
    //   this.kinveyService.storeFileToGoogleCloud(res["_uploadURL"], this.file).subscribe((res) => {
    //     console.log("creating image successfull");
    //     console.log(res["_uploadURL"]);
    //     console.log(res)
    //   });
    // });


    this.kinveyService.createPatient(this.model).subscribe(suc => {
      console.log(suc);
      if (this.model.bed_id.trim() !== '') {
        this.kinveyService.getBed(suc.bed_id).subscribe(bed => {
          bed.used = 1;
          this.kinveyService.updateBed(bed, bed._id).subscribe(response => {
            console.log(response);
          }
          );
        });
      }
    },
      err => {
        console.log(err);
      });

    return false;
  }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }


  ngOnInit() {

    // Preselect bed id
    this.model.bed_id = this.route.snapshot.paramMap.get('bed_id');

    this.kinveyService.getFreeRooms(this.model.bed_id).subscribe(res => {
      res.forEach(obj => {
        let foundroom = false;
        this.rooms.forEach((fr, idx) => {
          if (fr.number === obj.room_number.toString()) {
            foundroom = true;
            this.rooms[idx].beds.push({
              id: obj._id,
              number: obj.number.toString()
            });
          }
        });

        if (!foundroom) {
          const newobj: Room = {
            number: obj.room_number.toString(),
            beds: [{
              id: obj._id,
              number: obj.number.toString()
            }]
          };
          this.rooms.push(newobj);
        }
      });
    });
  }
}


