import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { KinveyApiService } from '../kinvey-api.service';
import {Patient} from '@app/patient';

export interface Bedpatient {
  id: string;
  number: string;
  patient: Patient;
}

export interface Roomlist {
  number: string;
  beds: Bedpatient[];
}

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {
  rooms: Roomlist[] = [];
  viewrooms: Roomlist[] = [];
  version: string = environment.version;
  constructor(private kinveyService: KinveyApiService) { }

  ngOnInit() {
    const taken_bed_ids: string[] = [];
    this.kinveyService.getBeds().subscribe(res => {
      res.forEach( obj => {
        let foundroom = false;
        this.rooms.forEach((fr, idx) => {
          if (fr.number === obj.room_number.toString()) {
            foundroom = true;
            this.rooms[idx].beds.push({
              id: obj._id,
              number: obj.number.toString(),
              patient: null
            });
          }
        });
        if (!foundroom) {
          const newobj: Roomlist = {
            number: obj.room_number.toString(),
            beds: [{
              id: obj._id,
              number: obj.number.toString(),
              patient: null
            }]
          };
          this.rooms.push(newobj);
        }
        if (obj.used === 1) {
          taken_bed_ids.push(obj._id);
        }
      });
      if (taken_bed_ids.length) {
        this.kinveyService.getPatientsByBeds(taken_bed_ids).subscribe(pbeds => {
          pbeds.forEach(pat => {
            this.rooms.forEach((r, ix) => {
              r.beds.forEach((b, bi) => {
                if (b.id === pat.bed_id) {
                  this.rooms[ix].beds[bi].patient = pat;
                }
              });
            });
          });
          this.viewrooms = this.rooms;
        });
      }
    });
  }
}
