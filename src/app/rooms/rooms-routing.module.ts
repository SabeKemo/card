import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { RoomsComponent } from './rooms.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'rooms',
      component: RoomsComponent,
      data: {
        title: extract('Availability')
      },
    }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class RoomsRoutingModule { }
