import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bed } from './bed';
import { Patient } from './patient';
import { Observable } from 'rxjs';
import { Uploaduri } from '@app/uploaduri';

const appKey = 'kid_HkzvljErV';
const appSecret = '4a02dec193a24320b200e5fbe1753bef';


@Injectable({
  providedIn: 'root'
})

export class KinveyApiService {

  authorizationHeader = 'Basic a2lkX0hrenZsakVyVjo1N2NkMTEyZjA2NWM0ZDg1YTA3N2M1YjA1ZmVhMDkwNQ==';

  public createPatient(patient: Patient) {
    return this.httpClient.post<Patient>(`${this.apiURL}/patients`, patient, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public updatePatient(patient: Patient, id: string) {
    return this.httpClient.put<Patient>(`${this.apiURL}/patients/${id}`, patient, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getPatient(id: string) {
    return this.httpClient.get<Patient>(`${this.apiURL}/patients/${id}`, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public updateBed(bed: Bed, id: string) {

    return this.httpClient.put<Bed>(`${this.apiURL}/beds/${id}`, bed, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getPatientByName(name: string) {
    return this.httpClient.get<Patient[]>(`${this.apiURL}/patients?query={"name":{"$regex":"^${name}" }}`, {

      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getFreeRooms(personal_bed_id: any) {
    let qaddress;
    if (typeof personal_bed_id === 'undefined' || personal_bed_id === null || personal_bed_id.trim() === '') {
      qaddress = `${this.apiURL}/beds?query={"used":0}&sort={"room_number":1,"number":1}`;
    } else {
      qaddress = `${this.apiURL}/beds?query={"$or":[{"used":0}, {"bed_id":"${personal_bed_id}"}]}&sort={"room_number":1,"number":1}`;
    }
    return this.httpClient.get<Bed[]>(qaddress, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getBed(id: string) {
    return this.httpClient.get<Bed>(`${this.apiURL}/beds/${id}`, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getBeds() {
    return this.httpClient.get<Bed[]>(`${this.apiURL}/beds?sort={"room_number":1,"number":1}`, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getPatientsByBeds(beds: string[]) {
    const $query_obj = {
      'bed_id': {
        '$in': [
          ...beds
        ]
      }
    };
    const json_string = JSON.stringify($query_obj);
    return this.httpClient.get<Patient[]>(`${this.apiURL}/patients?query=${json_string}`, {
      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public getPatients(): Observable<Patient[]> {

    return this.httpClient.get<Patient[]>(`${this.apiURL}/patients?sort={"name":1}`, {

      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json'
      }
    });
  }

  public storeFileToGoogleCloud(url: string, file: any) {
    return this.httpClient.put(url, file);
  }

  public getUploadUri() {
    return this.httpClient.post<Uploaduri>(`${this.apiBase}blob/${appKey}`, {}, {

      headers: {
        'Authorization': this.authorizationHeader,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type'
      }
    });
  }

  apiBase: string = 'https://baas.kinvey.com/';
  apiURL: string = this.apiBase + 'appdata/' + appKey;

  constructor(private httpClient: HttpClient) { }

}
